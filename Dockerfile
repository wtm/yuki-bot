FROM node:lts
WORKDIR /usr/src/app
COPY . .
RUN npm ci
RUN npm run build
CMD ["./dist/src/index.js"]
