from dataclasses import dataclass
import json
from dotenv import load_dotenv
load_dotenv()

from os import environ
OPENAI_API_KEY = environ.get("OPENAI_APIKEY")
UID_HASH_SALT = environ.get("UID_HASH_SALT")
MAX_TOKENS = 2048
MAX_PROMPT_LEN = 10000


from httpx import AsyncClient
client = AsyncClient(headers={
  "Authorization": f"Bearer {OPENAI_API_KEY}",
}, timeout=20)

from hashlib import sha256
import logging
log = logging.getLogger("uvicorn")
log.setLevel(logging.DEBUG);

from fastapi import FastAPI, HTTPException
app = FastAPI()

def hash_user_id(uid: str) -> str:
  h = sha256((UID_HASH_SALT + uid).encode("utf8"))
  b = h.digest()
  return bytes.hex(b)

@dataclass
class CompletionResult:
  result: str
  token_usage: int

@app.post("/complete")
async def complete(uid: str, data: dict):
  data["user"] = hash_user_id(uid)
  data["stream"] = False
  data["n"] = 1
  model = data['model']
  if data["max_tokens"] > MAX_TOKENS:
    raise HTTPException(400, "max_tokens too large")
  if len(data["prompt"]) > MAX_PROMPT_LEN:
    raise HTTPException(400, "prompt too long")
  res = await client.post("https://api.openai.com/v1/completions", json=data)
  if not res.is_success:
    raise HTTPException(500, f"openai API request failed: {res.status_code} {res.text}")
  else:
    res_json = res.json()
    cr = CompletionResult(
      result=res_json["choices"][0]["text"],
      token_usage=res_json["usage"]["total_tokens"]
    )
    log.info("%s", f"{uid} completed {json.dumps(cr.result)}, using {cr.token_usage} tokens with {model}.")
    log.debug("%s", data["prompt"])
    return cr
