/*

Error [ERR_REQUIRE_ESM]: require() of ES Module .../node_modules/node-fetch/src/index.js from .../src/ai.ts not supported.
Instead change the require of index.js in .../src/ai.ts to a dynamic import() which is available in all CommonJS modules.

*/

export const fetch = (...args) => eval(`import("node-fetch")`).then(module => module.default(...args))
