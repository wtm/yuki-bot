import { createLogger, format, transports } from "winston";
const logger = createLogger({
  level: "debug",
  format: format.cli({ all: true }),
  transports: [new transports.Console()]
});

export default logger;
