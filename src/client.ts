
import { Client, Intents } from "discord.js";

export const c = new Client({
  intents: [
    Intents.FLAGS.GUILDS, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILD_MESSAGES
  ],
  presence: {
    activities: [
      {
        type: "PLAYING",
        name: "Thought Entity"
      }
    ]
  },
  partials: [
    "CHANNEL", "MESSAGE", "REACTION"
  ]
});

export default c;
