import { ApplicationCommandType } from "discord-api-types/v10";
import { CommandInteraction, GuildTextBasedChannel, MessageActionRow, MessageButton } from "discord.js";

import c from "../client";
import type { IDiscordCommand } from "./index";
import logger from "../log";

export const command: IDiscordCommand = {
  metadata: {
    name: "ping",
    type: ApplicationCommandType.ChatInput,
    description: "Ping the bot."
  },
  handle
};
export default command;

export async function handle(int: CommandInteraction) {
  await int.editReply("Pong.");
}
