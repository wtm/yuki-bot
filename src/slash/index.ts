import { rest, Routes } from "./rest";
import { SELF_ID, YUKI_UNIMPRESSED } from "../consts";
import mktemp from "./mktemp";
import ping from "./ping";
import { CommandInteraction, Interaction } from "discord.js";

export interface IDiscordCommand {
  metadata: any,
  handle: (int: CommandInteraction) => Promise<void>,
}

const commands = [ mktemp, ping ];
import logger from "../log";

export async function pushGlobalCommands() {
  let json = [];
  let names = new Set();
  for (let c of commands) {
    json.push(Object.assign({}, c.metadata, {
      application_id: SELF_ID
    }));
    names.add(c.metadata.name);
  }
  await rest.put(
    Routes.applicationCommands(SELF_ID),
    { body: json }
  );
  let existing_cmds = await rest.get(Routes.applicationCommands(SELF_ID)) as any;
  for (let c of existing_cmds) {
    if (!names.has(c.name)) {
      await rest.delete(Routes.applicationCommand(SELF_ID, c.id));
    }
  }
}


export async function onInteractionCreate(evt: Interaction) {
  if (!evt.isApplicationCommand()) {
    return;
  }
  let int = evt as CommandInteraction;
  logger.info(`onInteractionCreate by ${evt.user.id} (${evt.user.username}#${evt.user.tag}) in ${evt.channelId}`);
  try {
    await int.deferReply();
  } catch (e) { }
  try {
    for (let c of commands) {
      if (int.commandName.toLowerCase() === c.metadata.name.toLowerCase()) {
        await c.handle(int);
        break;
      }
    }
  } catch (e) {
    logger.error(e);
    let text = `${YUKI_UNIMPRESSED}\n\`\`\`\n${e}\n\`\`\``;
    try {
      await Promise.any([
        int.reply(text),
        int.editReply(text)
      ]);
    } catch (e) {
      await int.followUp(text);
    }
  }
}
