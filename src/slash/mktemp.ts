import { ApplicationCommandType } from "discord-api-types/v10";
import { CommandInteraction, GuildTextBasedChannel, MessageActionRow, MessageButton } from "discord.js";

import c from "../client";
import type { IDiscordCommand } from "./index";
import logger from "../log";

export const command: IDiscordCommand = {
  metadata: {
    name: "mktemp",
    type: ApplicationCommandType.ChatInput,
    description: "Create a temporary server in which you have normal user permissions (i.e. not owner)."
  },
  handle
};
export default command;

const DELETE_TIME = 600 * 1000;

async function handle(int: CommandInteraction) {
  let g = await c.guilds.create("temp guild 001", {
    channels: [
      {
        name: "default",
        type: "GUILD_TEXT"
      }
    ]
  });
  let chan = (await g.channels.cache.at(0)) as GuildTextBasedChannel;
  let delete_msg = await chan.send({
    content: `**This server will be automatically deleted <t:${Math.round((Date.now() + DELETE_TIME) / 1000)}:R>.**`,
    components: [
      new MessageActionRow()
        .addComponents(new MessageButton().setStyle("DANGER").setLabel("Delete now").setCustomId("delete"))
        .addComponents(new MessageButton().setStyle("SUCCESS").setLabel(`Transfer ownership to @${int.user.username}`).setCustomId("transfer"))
    ]
  });
  let delete_collector = chan.createMessageComponentCollector({
    time: DELETE_TIME,
    filter: i => i.message.id === delete_msg.id
  });
  let delete_timeout = setTimeout(async () => {
    try {
      await g.delete();
    } catch (e) {
      logger.error(e);
    }
  }, DELETE_TIME);
  let seen = new Set();
  delete_collector.on("collect", async i => {
    if (seen.has(i.id)) {
      return;
    }
    seen.add(i.id);
    try {
      if (i.user.id === int.user.id) {
        if (i.customId === "delete") {
          await g.delete();
          clearTimeout(delete_timeout);
          delete_collector.stop();
        } else if (i.customId === "transfer") {
          await g.setOwner(int.user.id);
          clearTimeout(delete_timeout);
          delete_collector.stop();
          await delete_msg.delete();
        }
      } else {
        await i.reply({
          content: "No permission",
          ephemeral: true
        });
      }
    } catch (e) {
      logger.error(e);
    }
  });
  let inv = await g.invites.create(chan.id);
  await g.setSystemChannel(chan.id);
  await int.editReply(inv.url);
}
