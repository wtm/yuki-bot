import { REST } from "@discordjs/rest";
import { env } from "process";
const DISCORD_BOT_TOKEN = env.DISCORD_BOT_TOKEN;
export { Routes } from "discord-api-types/v10";
export const rest = new REST({ version: "10" }).setToken(DISCORD_BOT_TOKEN);
