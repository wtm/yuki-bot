import { env } from "process";
const OPENAI_GATEWAY = env.OPENAI_GATEWAY;
import { fetch } from "./import_node_fetch";

export async function complete(uid: string, data: {
  model?: string, prompt: string, temperature: number, frequency_penalty: number, presence_penalty: number, stop: string[], max_tokens?: number
}): Promise<string> {
  let r = await fetch(OPENAI_GATEWAY + "/complete?uid=" + encodeURIComponent(uid), {
    method: "POST",
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(Object.assign({}, {
      "model": "text-davinci-003",
      "max_tokens": 2048,
      "top_p": 1,
      "temperature": 0.5,
    }, data))
  });
  if (!r.ok) {
    throw new Error((await r.json())["detail"])
  }
  let res = (await r.json())["result"] as string;
  return res.trim();
}
