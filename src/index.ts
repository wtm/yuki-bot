require("dotenv").config();

import { GuildTextBasedChannel, Intents } from "discord.js";

import hooks from "./msg_hooks";
import { set_self_id, EXCLAMATION, INIT, SELF_MENTION_RE, STATE_CHANNEL, SELF_ID } from "./consts";
import logger from "./log";
import { init_sandbox } from "./msg_hooks/code"
import { pushGlobalCommands, onInteractionCreate } from "./slash"

import c from "./client";

Promise.all([
  init_sandbox(),
  new Promise((resolve) => c.on("ready", resolve)),
  c.login(process.env.DISCORD_BOT_TOKEN),
]).then(async () => {
  set_self_id(c.user.id);
  await pushGlobalCommands();
  logger.info("Bot ready!");
  for (let [name, g] of c.guilds.cache) {
    let perms = g.me.permissions;
    if (g.ownerId === SELF_ID) {
      await g.delete();
      logger.info("Deleted " + g.name);
      continue;
    }
    let perms_str = perms.toArray();
    logger.debug(`This bot is in ${name}, with permission ${perms_str.join(", ")}.`);
  }
  let state_chan = (await c.channels.fetch(STATE_CHANNEL)) as GuildTextBasedChannel;
  state_chan.send(`Bot (re)started (<t:${Math.floor(Date.now()/1000)}:R>)!`);

  const invite_link = c.generateInvite({
    scopes: ["bot", "applications.commands"],
    permissions: ["ADD_REACTIONS", "USE_EXTERNAL_EMOJIS", "VIEW_CHANNEL", "SEND_MESSAGES", "EMBED_LINKS"]
  });

  let handled = new Set();

  c.on("messageCreate", async msg => {
    if (msg.author.bot || msg.system) {
      return;
    }
    if (msg.partial) {
      msg = await msg.fetch();
    }
    let chan = msg.channel;
    let content = msg.content;
    let mentions_self = false;
    if (chan.type === "DM") {
      mentions_self = true;
    }
    if (SELF_MENTION_RE.test(content)) {
      mentions_self = true;
      content = content.replace(SELF_MENTION_RE, "");
    }
    if (!mentions_self) {
      return;
    }
    if (handled.has(msg.id)) {
      return;
    }
    handled.add(msg.id);
    let typing = chan.sendTyping();
    try {
      logger.debug(`Message received: ${msg.id} from ${msg.author.id} (${msg.author.tag}) in ${chan.id}: ${msg.content}`);
      if (await hooks({
        message: msg,
        content
      })) {
        return;
      }
      await typing;
      await msg.reply(`Hi <@${msg.author.id}> ${INIT}`);
      await msg.channel.send("This bot supports the following explicit usages via text chat:\n" +
                      "Get raw content of your message: raw + anything\n" +
                      "Execute code in a sandbox: just send me a code block with language set to either javascript (js) or sh/bash.\n" +
                      "\n" +
                      "In all other cases, I will try to guess what you want to do or chat with you as Yuki Nagato.\n" +
                      "\n" +
                      "All messages must mention me at the beginning if not in DM.");
      await msg.channel.send(`Invite me to your server: ${invite_link}`)
    } catch (e) {
      logger.error(`Error while handling ${msg.id}: ${e}`);
      state_chan.send(`${EXCLAMATION} There was an error when processing ${msg.url}:\n` + "```\n" + e + "\n```")
        .then(msg => {
          return msg.suppressEmbeds();
        }).catch(e => {});
      await Promise.all([
        msg.react(EXCLAMATION),
        msg.reply("There was an error when processing this message :(\n```\n" + e + "\n```")
      ]);
    }
  });

  c.on("interactionCreate", evt => {
    if (handled.has(evt.id)) {
      return;
    }
    handled.add(evt.id);
    onInteractionCreate(evt);
  });
});

c.login(process.env.DISCORD_BOT_TOKEN);
