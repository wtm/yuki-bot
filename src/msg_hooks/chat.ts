import type { HandleMsg } from "./index";

import { EXCLAMATION, INIT, YUKI_UNIMPRESSED } from "../consts";
import { Message } from "discord.js";

import { complete } from "../ai";

const prompt_start = `\
You are Yuki Nagato from the anime "Suzumiya Haruhi no Yuuutsu", having a discord chat with another person. Just for context, Yuki Nagato is an alien created by the Thought Entity who has a lot of supernatural powers but not much emotion.

`;
const person_name = "Person";
const bot_name = "Yuki";

let user_contexts = new Map();

function make_context_token(msg: Message) {
  return `${msg.author.id} ${msg.channel.id}`;
}

export async function prehandle_chat({content, message: msg}: HandleMsg): Promise<boolean> {
  if (content.toLowerCase() === "clear context") {
    user_contexts.delete(make_context_token(msg));
    msg.react(INIT);
    msg.reply("Context cleared. I have now forgotten all your previous messages.");
    return true;
  }
  return false;
}

export async function handle_chat(content: string, msg: Message) {
  let uid = msg.author.id;
  let ctx_tok = make_context_token(msg);
  let pbuf: string[] = [prompt_start];
  let ctx: string[];
  if (user_contexts.has(ctx_tok)) {
    ctx = user_contexts.get(ctx_tok);
    ctx.push(`${person_name}: ${content}`);
    if (ctx.length > 16) {
      ctx = ctx.slice(-16);
      pbuf.push("...");
    }
    for (let line of ctx) {
      pbuf.push(line);
    }
  } else {
    ctx = [`${person_name}: ${content}`];
    pbuf.push(ctx[0]);
  }
  user_contexts.set(ctx_tok, ctx);
  try {
    let cres = await complete(uid, {
      prompt: pbuf.join("\n") + `\n${bot_name}:`,
      stop: [`${person_name}:`],
      frequency_penalty: 1.5,
      presence_penalty: 0.5,
      temperature: 1
    });
    ctx.push(`${bot_name}: ${cres}`);
    await msg.channel.send(cres);
  } catch (e) {
    await Promise.all([
      msg.react(EXCLAMATION),
      msg.reply(`An error had occured, and thus I'm unable to reply right now ${YUKI_UNIMPRESSED}\n${e}`)
    ]);
  }
}

export default async function handle({ message: msg, content }: HandleMsg): Promise<boolean> {
  await handle_chat(content, msg);
  return true;
}
