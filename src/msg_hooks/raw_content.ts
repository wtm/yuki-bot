import { Message } from "discord.js";
import { SELF_ID, INIT } from "../consts";

import type { HandleMsg } from "./index";

export default async function handle({ message: msg, content }: HandleMsg): Promise<boolean> {
  if (!/^raw\s/.test(content)) {
    return false;
  }
  let raw_content = "```\n" + msg.content + "\n```";
  await msg.channel.send({
    content: `The raw content of your message is:\n${raw_content}`,
    reply: {
      failIfNotExists: false,
      messageReference: msg
    }
  });
  await msg.react(INIT);
  return true;
}
