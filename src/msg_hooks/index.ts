import { Message } from "discord.js";
import code_hook from "./code";
import raw_content_hook from "./raw_content";
import ai_classify from "./ai_classify";
import { prehandle_chat } from "./chat";

export interface HandleMsg {
  message: Message,
  content: string
}

export default async function handle(m: HandleMsg) {
  if (await prehandle_chat(m)) {
    return true;
  }
  if (await code_hook(m)) {
    return true;
  }
  if (await raw_content_hook(m)) {
    return true;
  }
  return await ai_classify(m);
}
