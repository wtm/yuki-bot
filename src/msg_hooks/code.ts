import { NO_ENTRY, YUKI_UNIMPRESSED, INIT, QUESTION, EXCLAMATION, HOURGLASS } from "../consts";
import logger from "../log";
import { exec } from "child_process";
import { Message, MessageAttachment, MessageReaction } from "discord.js";
import type { HandleMsg } from "./index";

import { ProcschdServer } from "procschd";
import { env } from "process";

let sandbox: ProcschdServer = null;

export async function init_sandbox() {
  try {
    sandbox = await ProcschdServer.connect(env.SANDBOX_SERVER, env.SANDBOX_AUTH_TOKEN);
  } catch (e) {
    throw new Error("Unable to initalize sandbox: " + e);
  }
}

const CODEBLOCK_RE = /^\s*```([a-z0-9]+)\n([\s\S]*)```\s*$/i;
const CODEBLOCK_RE_NOLANG = /^\s*```([\s\S]*)```\s*$/i;

async function run_simple(image: string, code: string): Promise<[boolean, string]> {
  let reqid = await sandbox.runTask({
    imageId: image,
    stdin: code
  });
  let res = await sandbox.taskInfo(reqid, true);
  if (res.error) {
    throw new Error(res.error);
  }
  let success = true;
  let result = res.stdout;
  if (res.stderr.trim()) {
    success = false;
    result += res.stderr;
  }
  if (result.trim().length === 0) {
    result = "(empty output)";
  }
  return [success, result];
}

export async function handle_code(language: string, code: string, msg: Message) {
  let reacted_with_hourglass: Promise<MessageReaction> | null = null;
  function clearHourglassReaction() {
    if (reacted_with_hourglass) {
      let p = reacted_with_hourglass;
      reacted_with_hourglass = null;
      return p.then(r => r.users.remove(msg.client.user));
    } else {
      return Promise.resolve();
    }
  }
  try {
    let success: boolean, result: string;
    let hourglass_timeout = setTimeout(() => {
      reacted_with_hourglass = msg.react(HOURGLASS);
    }, 1000);
    if (["javascript", "js"].includes(language)) {
      [success, result] = await run_simple("sandbox/node-p", code);
    } else if (["sh", "bash", "shell"].includes(language)) {
      [success, result] = await run_simple("sandbox/sh", code);
    } else {
      success = false;
      result = "Invalid language " + language;
    }
    clearTimeout(hourglass_timeout);
    await Promise.all([
      msg.react(success ? INIT : YUKI_UNIMPRESSED),
      (async () => {
        if (result.length < 1000 && result.indexOf("```") < 0) {
          await msg.reply("```\n" + result + "\n```");
        } else {
          await msg.reply({
            files: [{
              name: "output.txt",
              attachment: Buffer.from(result, "utf-8")
            }]
          });
        }
      })(),
      clearHourglassReaction(),
    ]);
  } catch (e) {
    await clearHourglassReaction();
    throw new Error(`An error occured when trying to invoke the sandbox: ${e}`);
  }
}


export default async function handle({ message: msg, content }: HandleMsg): Promise<boolean> {
  let match = content.match(CODEBLOCK_RE);
  if (match) {
    let language = match[1];
    let code = match[2];
    await handle_code(language, code, msg);
    return true;
  } else if (CODEBLOCK_RE_NOLANG.test(content)) {
    await Promise.all([
      msg.react(QUESTION),
      msg.reply("Please specify a language with ```lang.")
    ]);
    return true;
  }
  return false;
}
