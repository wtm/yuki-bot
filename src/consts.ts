export const INIT = "<:init:988968332181000212>";
export const NO_ENTRY = "🚫";
export const YUKI_UNIMPRESSED = "<:YukiUnimpressed:988971633211502592>";
export const EXCLAMATION = "❗";
export const QUESTION = "❔";
export const HOURGLASS = "⏳";

export * from "../config";

export let SELF_ID: string = "?";
export let SELF_MENTION_RE: RegExp = /\?/g;

export function set_self_id(self_id: string) {
  SELF_ID = self_id;
  SELF_MENTION_RE = new RegExp(`^\\s*<@${SELF_ID}>\\s*|\\s*<@${SELF_ID}>\\s*$`, "g");
}
